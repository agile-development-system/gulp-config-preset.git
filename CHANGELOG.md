## [1.0.4](https://gitee.com/agile-development-system/gulp-config-preset/compare/0cd08cbf96f8eed03bb3e6345e3baf1b86c284e6...v1.0.4) (2022-06-14)


### Bug Fixes

* 修复预设问题 ([d589e56](https://gitee.com/agile-development-system/gulp-config-preset/commits/d589e56ef7c2949bd661118641413aaff75c61f9))
* 修复cp任务的文件判断条件 ([1de90f2](https://gitee.com/agile-development-system/gulp-config-preset/commits/1de90f2781deaaaaa2a3535cbf711d1adac60ccd))
* 支持项目根目录不存在tsconfig时调用默认ts配置,避免js项目配置tsconfig导致的奇怪问题 ([9d2d2f9](https://gitee.com/agile-development-system/gulp-config-preset/commits/9d2d2f91228b5ba43fdaa84a3a1bb8c1e850bb42))
* **package.json:** 补充依赖 ([7055967](https://gitee.com/agile-development-system/gulp-config-preset/commits/7055967d991edb8d6febd2249be88ae7e02d479c))


### Features

* init ([0cd08cb](https://gitee.com/agile-development-system/gulp-config-preset/commits/0cd08cbf96f8eed03bb3e6345e3baf1b86c284e6))



